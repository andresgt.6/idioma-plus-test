# Idioma plus Test

Hola, en Assets te adjunto una foto del diagrama de clases que utilizé,en esta prueba no me
basé tanto en terminar la mayor cosas posibles si no en hacer todo para que el proyecto crezca
de manera fácil y rápida, en resumen me enfonqué más en las raices, en el proyecto estoy usando
2 Assets, LeanTween por que me parece que facilita mucho las cosas y para animar UIs ya que no 
se pueden hacer animaciones en canvas y el otro Asset es FinalIk, lo puse por que con este Asset
se puede hacer que todas las animaciones, interacciones etc se vean mucho más naturales y aporta un
realismo y le da más peso a un proyecto sin tener que hacer mucho.


lo que alcancé a hacer:

-Sistema de guardar y cargar partidas, mostrando sus datos.
-Sistema de Movimiento del jugador usando IK.
-Sistema de carga.

Agradecería mucho que así no pase me puedas dar una retroalimentación de que consideras que está mal
así sea corta y puntual, ya que esto me ayudaría muchísimo como desarrollador, aunque no es obligación
yo sé que liderar un proyecto te roba todo el tiempo, ahora liderar dos ni 
me imagino...

Hice lo que pude, le trabajé a la prueba después de todo un día de trabajo.

Quedo atento a cualquier cosa, muchas gracias!