public enum MainButtonsTypes
{
    None,
    NewGame,
    LoadButton,
    ExitButton
}

public enum SceneToLoad
{
    Exit,
    UIHomeScene,
    GameplayScene
}
