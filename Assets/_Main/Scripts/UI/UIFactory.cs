﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFactory
{
    public static Canvas CanvasSave(Transform parent)
    {        
        GameObject currentUi = GameObject.Instantiate( Resources.Load<GameObject>("UIs/CanvasSave"),parent);                

        return currentUi.GetComponent<Canvas>();
    }      
}
