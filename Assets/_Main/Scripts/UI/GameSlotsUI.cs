using System;
using UnityEngine;
using UnityEngine.UI;

public class GameSlotsUI : MonoBehaviour
{
    public event Action<MainButtonsTypes,int> OnPressSlot;

    [Header("Texts")]
    [SerializeField] private Text mainText;

    [Header("UI elements")]
    [SerializeField] private SlotElements[] slots = new SlotElements[3];

    private Canvas canvas;

    private MainButtonsTypes currentMainButton;

    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        SetEnabled(MainButtonsTypes.None);
    }

    public void SetEnabled(MainButtonsTypes buttonPressed, GameData[] data = null)
    {
        currentMainButton = buttonPressed;

        bool disable = buttonPressed.Equals(MainButtonsTypes.None);
        canvas.enabled = !disable;

        if (disable || data == null)
            return;

        switch (buttonPressed)
        {            
            case MainButtonsTypes.NewGame:
                mainText.text = "New Game";
                break;
            case MainButtonsTypes.LoadButton:
                mainText.text = "Continue";
                break;            
        }

        for (int i = 0; i < slots.Length; i++)
        {            
            if(data[i] != null)
            {
                slots[i].SetScreen(true);

                slots[i].slotText.text = "Slot" + (data[i].id + 1);
                slots[i].playedGamesText.text = "Played Games:" + data[i].playedGames;
                slots[i].wonGamesText.text = "Won Games:" + data[i].wonGames;
                slots[i].lostGamesText.text = "Lost Games:" + data[i].lostGames;

                slots[i].SetTime(data[i].playedTime);
            }
            else
            {
                slots[i].SetScreen(false);
            }
        }
    }

    public void OnPressSlotButton(int slotId)
    {
        OnPressSlot?.Invoke(currentMainButton, slotId);
    }
}

[System.Serializable]
public class SlotElements
{
    [Header("Canvas groups")]
    [SerializeField] private CanvasGroup dataFoundScreen; 
    [SerializeField] private CanvasGroup dataEmptyScreen;

    [Header("Texts")]
    public Text slotText;
    public Text playedGamesText;
    public Text wonGamesText;
    public Text lostGamesText;
    [SerializeField] private Text playedTimeText;

    public void SetScreen(bool dataFound)
    {
        dataFoundScreen.alpha = dataFound ? 1 : 0;
        dataEmptyScreen.alpha = dataFound ? 0 : 1;
    }

    public void SetTime(float time)
    {
        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = (time % 60).ToString("00");
        playedTimeText.text = string.Format("Played Time {0}:{1}", minutes, seconds);
    }
}
