using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
    [Header("Images")]
    [SerializeField] private Image wheel;
    [SerializeField] private Image circleFill;

    [Header("Colors")]
    [SerializeField] private Color startedColor;
    [SerializeField] private Color finalColor;

    private Canvas canvas;

    public float currentValue;
    private float maxAmount;

    private void Awake()
    {
        canvas = GetComponent<Canvas>();

        SetLoading(false);
    }

    public void SetLoading(bool enabled, float maxAmount = 1)
    {
        canvas.enabled = enabled;
        this.maxAmount = maxAmount;

        if (enabled)
        {
            UpdateProgress(0);
            StartRotating();
        }
        else
        {
            LeanTween.cancel(wheel.gameObject);
        }
    }

    private void StartRotating()
    {
        LeanTween.rotateAroundLocal(wheel.gameObject, new Vector3(0, 0, 1), -360, 1).setOnComplete(StartRotating);
    }

    public void UpdateProgress(float progress)
    {
        circleFill.transform.localScale = new Vector3(progress / maxAmount, progress / maxAmount);
        circleFill.color = Color.Lerp(startedColor, finalColor, progress / maxAmount);
    }
}
