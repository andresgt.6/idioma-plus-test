using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progress
{
    public const string IDPATH = "Id";
    public const string PLAYEDGAMESPATH = "PlayedGames";
    public const string WONPATH = "WonGames";
    public const string LOSTGAMESPATH = "LostGames";
    public const string PLAYEDTIMEPATH = "PlayedTime";    

    public GameData[] SaveOrLoadGameDatas
    {
        get 
        {
            GameData[] data = new GameData[3];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = LoadGameData(i);
            }
            return data;
        }
        set
        {
            SaveData(value);
        }
    }

    private GameData LoadGameData(int id)
    {
        if(!SlotHasValue(id))
        {
            return null;
        }
        else
        {
            GameData data = new GameData();
            data.id = PlayerPrefs.GetInt(string.Format("{0}/{1}", id, IDPATH));
            data.playedGames = PlayerPrefs.GetInt(string.Format("{0}/{1}", id, PLAYEDGAMESPATH));
            data.playedGames = PlayerPrefs.GetInt(string.Format("{0}/{1}", id, PLAYEDGAMESPATH));
            data.playedGames = PlayerPrefs.GetInt(string.Format("{0}/{1}", id, LOSTGAMESPATH));
            data.playedTime = PlayerPrefs.GetFloat(string.Format("{0}/{1}", id, PLAYEDTIMEPATH));
            return data;
        }
    }

    public void SaveData(GameData[] data)
    {
        for (int i = 0; i < data.Length; i++)
        {
            GameData currentData = data[i];
            PlayerPrefs.SetInt(string.Format("{0}/{1}", i, IDPATH), currentData.id);
            PlayerPrefs.SetInt(string.Format("{0}/{1}", i, PLAYEDGAMESPATH), currentData.playedGames);
            PlayerPrefs.SetInt(string.Format("{0}/{1}", i, WONPATH), currentData.wonGames);
            PlayerPrefs.SetInt(string.Format("{0}/{1}", i, LOSTGAMESPATH), currentData.lostGames);
            PlayerPrefs.SetFloat(string.Format("{0}/{1}", i, PLAYEDTIMEPATH), currentData.playedTime);
        }
    }

    public void SaveData(GameData data)
    {
        PlayerPrefs.SetInt(string.Format("{0}/{1}", data.id, IDPATH), data.id);
        PlayerPrefs.SetInt(string.Format("{0}/{1}", data.id, PLAYEDGAMESPATH), data.playedGames);
        PlayerPrefs.SetInt(string.Format("{0}/{1}", data.id, WONPATH), data.wonGames);
        PlayerPrefs.SetInt(string.Format("{0}/{1}", data.id, LOSTGAMESPATH), data.lostGames);
        PlayerPrefs.SetFloat(string.Format("{0}/{1}", data.id, PLAYEDTIMEPATH), data.playedTime);
    }

    public bool SlotHasValue(int slotId)
    {
        return PlayerPrefs.HasKey(string.Format("{0}/{1}", slotId, IDPATH));
    }

    public void DeleteData(int? idToDelete)
    {
        if(!idToDelete.HasValue)
        {
            PlayerPrefs.DeleteAll();
        }
        else
        {
            if (!SlotHasValue(idToDelete.Value))
                return;

            PlayerPrefs.DeleteKey(string.Format("{0}/{1}", idToDelete, IDPATH));
            PlayerPrefs.DeleteKey(string.Format("{0}/{1}", idToDelete, PLAYEDGAMESPATH));
            PlayerPrefs.DeleteKey(string.Format("{0}/{1}", idToDelete, WONPATH));
            PlayerPrefs.DeleteKey(string.Format("{0}/{1}", idToDelete, LOSTGAMESPATH));
            PlayerPrefs.DeleteKey(string.Format("{0}/{1}", idToDelete, PLAYEDTIMEPATH));
        }
    }
}
