

[System.Serializable]
public class GameData
{
    public int id;
    public int playedGames;
    public int wonGames;
    public int lostGames;
    public float playedTime;

    public GameData()
    {
        
    }

    public GameData(int id)
    {
        this.id = id;
    }
}
