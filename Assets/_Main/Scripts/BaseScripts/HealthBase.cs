using System;
using UnityEngine;

public class HealthBase : MonoBehaviour
{
    public event Action OnDeath;

    protected float startHealth;

    public float Health { get; private set; } = 100;

    protected virtual void Awake()
    {
        startHealth = Health;
    }

    public virtual void TakeDamage(float damage = 10)
    {
        Health -= 10;

        if (Health <= 0)
            OnDeath?.Invoke();
    }

    //Use it with posions
    public void AddCustomHealth(float customValue)
    {
        Health += customValue;
    }

    protected virtual int GetHealthPercentage()
    {
        return (int)(Health / startHealth) * 100;
    }
}
