using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : HealthBase
{
    public event Action LowHealth;

    public override void TakeDamage(float damage = 10)
    {
        base.TakeDamage(damage);
        if (GetHealthPercentage() <= 20)
            LowHealth?.Invoke();
    }
}
