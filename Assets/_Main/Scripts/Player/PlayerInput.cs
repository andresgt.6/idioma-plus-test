using System;
using UnityEngine;

public enum PlayersInputs
{
    Attack
}

public class PlayerInput : MonoBehaviour
{
    public event Action<PlayersInputs> OnPlayerInputs;

    private bool isEnabled = true;

    public void SetEnabled(bool enabled)
    {
        isEnabled = enabled;
    }

    private void Update()
    {
        if (!isEnabled)
            return;

        if(Input.GetMouseButtonDown(0))
        {
            OnPlayerInputs?.Invoke(PlayersInputs.Attack);
        }
    }
}
