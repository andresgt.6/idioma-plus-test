using System;
using System.Collections;
using UnityEngine;
using RootMotion.Demos;
using RootMotion;

public enum PlayerState
{
    Disabled,
    Idle,
    Attack
}

public class PlayerController : MonoBehaviour
{
    public event Action OnPlayerDeath;

    [Header("Current State")]
    [SerializeField] private PlayerState currentState;

    public bool IsEnabled { get; private set; } = true;

    private Action OnCompleteInit;

    private PlayerInput inputs;

    private SimpleLocomotion locomotion;
    private CameraController cameraMovement;

    private AnimatorManager animatorManager;

    private void Awake()
    {
        inputs = GetComponent<PlayerInput>();
        inputs.OnPlayerInputs += OnPlayerPerformInput;

        locomotion = GetComponentInChildren<SimpleLocomotion>();
        cameraMovement = GetComponentInChildren<CameraController>();
        animatorManager = GetComponentInChildren<AnimatorManager>();

        Init(null); Debug.LogWarning("Auto init enabled, please didabled");
    }    

    public void Init(Action OnCompleteInit)
    {
        this.OnCompleteInit = OnCompleteInit;
        StartCoroutine(InitAllPlayerComponents());
    }

    private IEnumerator InitAllPlayerComponents()
    {
        locomotion.Init();
        cameraMovement.Init();

        yield return null;

        OnCompleteInit?.Invoke();
    }
    
    public void SetEnabled(bool enabled)
    {
        IsEnabled = enabled;
        locomotion.SetEnabled(enabled);
        cameraMovement.SetEnabled(enabled);
    }

    private void OnPlayerPerformInput(PlayersInputs performedInput)
    {
        switch (performedInput)
        {
            case PlayersInputs.Attack:
                currentState = PlayerState.Attack;
                animatorManager.ChangeAnimation(currentState, true, CompleteAnimationCallback:() => OnPlayerInputPerfomed(PlayersInputs.Attack));
                locomotion.SetEnabled(false);
                break;
            default:
                break;
        }        

        
    }

    private void OnPlayerInputPerfomed(PlayersInputs performedInput)
    {
        switch (performedInput)
        {
            case PlayersInputs.Attack:
                currentState = PlayerState.Idle;
                locomotion.SetEnabled(IsEnabled);
                break;
            default:
                break;
        }
    }
}
