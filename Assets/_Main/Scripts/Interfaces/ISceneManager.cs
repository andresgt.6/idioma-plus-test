using System;
using System.Collections.Generic;
using UnityEngine;

public interface ISceneManager
{
    public event Action<SceneToLoad, PlayerData> OnExit;

    public void Init(Config config,PlayerData playerData,Action OnCompleteInit);
}
