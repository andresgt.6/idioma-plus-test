using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Configuration/GameConfig")]
public class Config : ScriptableObject
{
    public float playerSpeed;
    public int playerLifes;

    public int enemiesNumber = 3;
    public float npcSpeed;
    public int npcLifes;
}
