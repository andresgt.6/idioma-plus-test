﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorManager : MonoBehaviour
{
	protected Animator animator;

	private Action CompleteAnimationCallback;

	protected virtual void Awake ()
	{
		animator = GetComponent <Animator> ();
		DeactiveAnimations ();
	}

	public virtual void ChangeAnimation<T> (T animation, bool isTrigger = false, bool activate = true, Action CompleteAnimationCallback = null)
	{
		DeactiveAnimations ();

		if (CompleteAnimationCallback != null)
			this.CompleteAnimationCallback = CompleteAnimationCallback;

		if (!isTrigger) {
			animator.SetBool (animation.ToString (), activate);
		} else {
			animator.SetTrigger (animation.ToString ());
		}
	}

	public virtual void ChangeAnimation<T>(T animation, float value)
	{
		DeactiveAnimations();
		animator.SetFloat(animation.ToString(), value);		
	}

	public virtual void DeactiveAnimations ()
	{        
        for (int i = 0; i < animator.parameterCount; i++)
            if (animator.parameters[i].type == AnimatorControllerParameterType.Bool)
                animator.SetBool(animator.parameters[i].name, false);
    }

    protected virtual void OnStartAnimation(string animationName)
    {

    }

    protected virtual void OnCompleteAnimation (string animationName)
	{
		CompleteAnimationCallback?.Invoke();
    }
    
}
