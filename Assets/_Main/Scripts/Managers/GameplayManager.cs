using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour,ISceneManager
{
    public event Action<SceneToLoad, PlayerData> OnExit;
    [SerializeField] private PlayerController player;

    private float currentTime;

    private bool isInit;

    private Progress progress = new Progress();

    private PlayerData playerData;

    private void Start()
    {
        AppManager.RegisterSceneManager?.Invoke(this);
    }

    public void Init(Config config, PlayerData playerData, Action OnCompleteInit)
    {
        isInit = true;

        player.Init(OnCompleteInit);

        this.playerData = playerData;

        currentTime = playerData.currentGameData.playedTime;

        LeanTween.delayedCall(10, () => SaveData());
    }

    private void Update()
    {
        if(isInit)
        {
            currentTime += Time.deltaTime;
        }
    }

    private void SaveData()
    {
        Canvas saveScreen = UIFactory.CanvasSave(null);
        Destroy(saveScreen.gameObject, 3);

        playerData.currentGameData.playedTime = currentTime;        
        progress.SaveData(playerData.currentGameData);                

        LeanTween.delayedCall(10, () => SaveData());
    }
}
