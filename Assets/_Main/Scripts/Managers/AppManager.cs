using System;
using UnityEngine;

public sealed class AppManager : MonoBehaviour
{
    public static Action<ISceneManager> RegisterSceneManager;

    [Header("Configuration")]
    [SerializeField] private Config configurations;

    [Header("Player Data")]
    [SerializeField] private PlayerData playerData;

    private ISceneManager currentSceneManager;

    private SceneLoaderManager sceneLoaderManager;

    private void Awake()
    {
        sceneLoaderManager = GetComponent<SceneLoaderManager>();

        RegisterSceneManager += OnRegisterSceneManager;

        DontDestroyOnLoad(gameObject);
    }

    private void OnRegisterSceneManager(ISceneManager sceneManager)
    {
        currentSceneManager = sceneManager;
        currentSceneManager.Init(configurations, playerData, OnCompleteOpenSceneManager);
        currentSceneManager.OnExit += CurrentSceneManager_OnExit;
    }

    private void OnCompleteOpenSceneManager()
    {
        LeanTween.delayedCall(1, () => sceneLoaderManager.DisableLoading());
    }

    private void CurrentSceneManager_OnExit(SceneToLoad sceneToLoad, PlayerData playerData)
    {
        if(playerData != null)
            this.playerData = playerData;

        currentSceneManager.OnExit -= CurrentSceneManager_OnExit;

        if(sceneToLoad.Equals(SceneToLoad.Exit))
        {
            OnExitApplication();
            return;
        }

        sceneLoaderManager.LoadScene(sceneToLoad, OnLoadScene);
    }

    private void OnLoadScene()
    {
        Debug.Log("Scene loaded!");
    }

    private void OnExitApplication()
    {
        Application.Quit();
    }
}
