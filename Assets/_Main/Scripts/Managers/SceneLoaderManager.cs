using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderManager : MonoBehaviour
{
    private LoadingUI loadingUI;

    private SceneToLoad currentScene;

    private Action completeLoading;

    private void Awake()
    {
        loadingUI = GetComponentInChildren<LoadingUI>();
    }

    public void LoadScene(SceneToLoad sceneToLoad, Action OnCompleteLoad)
    {
        loadingUI.SetLoading(true);
        completeLoading = OnCompleteLoad;
        currentScene = sceneToLoad;
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(.5f);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(currentScene.ToString());
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            loadingUI.UpdateProgress(asyncOperation.progress);
            if (asyncOperation.progress == 0.9f)
                asyncOperation.allowSceneActivation = true;              

            yield return null;
        }
        completeLoading?.Invoke();
        loadingUI.UpdateProgress(1);
    }

    public void DisableLoading()
    {
        loadingUI.SetLoading(false);
    }
}
