using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour,ISceneManager
{
    public event Action<SceneToLoad, PlayerData> OnExit;

    [Header("Main Elements")]
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button loadButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Canvas mainCanvas;

    [Header("UIs")]
    [SerializeField] private GameSlotsUI slotsUI;

    private Progress progress = new Progress();
    private PlayerData playerData;

    private void Awake()
    {
        newGameButton.onClick.AddListener(() => OnClickMainButton(MainButtonsTypes.NewGame));
        loadButton.onClick.AddListener(() => OnClickMainButton(MainButtonsTypes.LoadButton));
        exitButton.onClick.AddListener(() => OnClickMainButton(MainButtonsTypes.ExitButton));

        mainCanvas.enabled = false;

        slotsUI.OnPressSlot += OnPressSlot;
    }

    private void Start()
    {
        AppManager.RegisterSceneManager?.Invoke(this);
    }

    public void Init(Config config, PlayerData playerData, Action OnCompleteInit)
    {
        OnCompleteInit?.Invoke();//If needs to wait a process to init
        this.playerData = playerData;
        mainCanvas.enabled = true;
        EnabledContinueButton();
    }

    private void EnabledContinueButton()
    {
        bool enabledContinue = false;

        for (int i = 0; i < 3; i++)
        {
            if (progress.SlotHasValue(i))
            {
                enabledContinue = true;
                break;
            }
        }

        loadButton.gameObject.SetActive(enabledContinue);
    }

    private void OnClickMainButton(MainButtonsTypes buttonPressed)
    {
        switch (buttonPressed)
        {
            case MainButtonsTypes.NewGame:
            case MainButtonsTypes.LoadButton:
                slotsUI.SetEnabled(buttonPressed, progress.SaveOrLoadGameDatas);
                break;
            case MainButtonsTypes.ExitButton:
                OnExit?.Invoke(SceneToLoad.Exit, null);
                break;            
        }

    }

    private void OnPressSlot(MainButtonsTypes buttonType, int slotId)
    {
        switch (buttonType)
        {            
            case MainButtonsTypes.NewGame:
                progress.DeleteData(slotId);
                break;
            case MainButtonsTypes.LoadButton:
                if (!progress.SlotHasValue(slotId))
                    return;
                break;            
        }

        slotsUI.OnPressSlot -= OnPressSlot;
        GameData dataToSet = progress.SaveOrLoadGameDatas[slotId];
        if (dataToSet == null)
            dataToSet = new GameData(slotId);
        playerData.currentGameData = dataToSet;
        OnExit?.Invoke(SceneToLoad.GameplayScene,playerData);
    }

}
